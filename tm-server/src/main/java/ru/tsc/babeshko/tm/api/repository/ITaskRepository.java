package ru.tsc.babeshko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String projectId);

}