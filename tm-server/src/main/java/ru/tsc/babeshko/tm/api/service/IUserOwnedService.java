package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.IUserOwnedRepository;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Sort sort);

}