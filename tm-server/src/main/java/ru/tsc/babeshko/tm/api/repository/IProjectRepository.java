package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

}