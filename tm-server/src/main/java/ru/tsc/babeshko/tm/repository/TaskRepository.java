package ru.tsc.babeshko.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String table = "tm_task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task fetch(@NotNull ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setDateBegin(row.getTimestamp("date_begin"));
        task.setDateEnd(row.getTimestamp("date_end"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, created, name, description, status, user_id, date_begin, date_end) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, task.getUserId());
            if (task.getDateBegin() != null)
                statement.setTimestamp(7, new Timestamp(task.getDateBegin().getTime()));
            if (task.getDateEnd() != null)
                statement.setTimestamp(8, new Timestamp(task.getDateEnd().getTime()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull String userId, @NotNull Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ?, project_id = ? WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT id, created, name, description, status, user_id, project_id, date_begin, date_end "
                        + "FROM %s WHERE user_id = ? AND project_id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @NotNull final String query = String.format("DELETE FROM %s WHERE project_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.executeUpdate();
        }
    }

}