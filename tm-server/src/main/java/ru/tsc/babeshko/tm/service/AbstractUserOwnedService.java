package ru.tsc.babeshko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.IUserOwnedRepository;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IUserOwnedService;
import ru.tsc.babeshko.tm.enumerated.Sort;
import ru.tsc.babeshko.tm.exception.entity.ModelNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@NotNull final String userId, @NotNull final M model) {
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId, sort.getComparator());
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneById(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M result;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            result = repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        if (result == null) throw new ModelNotFoundException();
        return result;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public long getCount(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getCount(userId);
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

}