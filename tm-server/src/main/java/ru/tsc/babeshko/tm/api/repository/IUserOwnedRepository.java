package ru.tsc.babeshko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    long getCount(@NotNull String userId);

    boolean existsById(@NotNull String userId, String id);

}